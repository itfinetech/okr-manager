# Objectives and Key Results (OKR) Manager

Objectives & Key Results (OKR) help us to achieve our goals. The OKR manager assists us in tracking and evaluating our success in regards to our OKRs.  
An objective is something we want to achieve. Key Results are our way of measuring our progress towards that goal.  
  
Objectives can be broad and fairly losely defined but should show something that is being strived for.  
Key Results make the progress measurable. Key Results are like goals, they:
* have a timeframe  
* are measurable  
* are achievable (realistic)  
  
A good Key Result is really close to unachievable. It should make the setter of the OKRs uncomfortable. If it is too easy, not enough effort will be made. If it is too hard, it will not be motivating enough to continue.  
  
OKRs are set for certain timeframes. A good timeframe would be a month or a quarter of a year.  
After that timeframe passes, the Key Results should be graded from 0.0 to 1.0 according to how well they were achieved. Once all Key Results of an Objective have been graded, the average of all Key Results is calculated and added as the overall grade of achievement for the Objective. Now the success towards an Objective has become achievable and measurable.
  
To not get stressed, people should not have more than 4-5 Objectives for any given timeframe. More than that will become too much and will result in burn-out, stress and lower performance.  
The same goes for Key Results. Too many Key Results per Objective can become difficult to keep track of and will demotivate quickly.  
  
## Example
Objective 1: I want to improve my programming  
&ensp;&ensp;&ensp;&ensp;|  
&ensp;&ensp;&ensp;&ensp;|- Key Result 1: Do M1m0 for 20 minutes every day for 30 days straight.  
&ensp;&ensp;&ensp;&ensp;|  
&ensp;&ensp;&ensp;&ensp;|- Key Result 2: Watch a 20 minute YouTube video about programming once a week.  
&ensp;&ensp;&ensp;&ensp;|  
&ensp;&ensp;&ensp;&ensp;|- Key Result 3: Work on a programming project for 30 minutes every 2 days minimum.  
  
After one month, the grades are as follows:  
  
Objective 1  
&ensp;&ensp;&ensp;&ensp;|  
&ensp;&ensp;&ensp;&ensp;|- Key Result 1: 0.7  
&ensp;&ensp;&ensp;&ensp;|  
&ensp;&ensp;&ensp;&ensp;|- Key Result 2: 0.5  
&ensp;&ensp;&ensp;&ensp;|  
&ensp;&ensp;&ensp;&ensp;|- Key Result 3: 0.2  
  
The average of the 3 Key Results is  

`(0.7 + 0.5 + 0.2) / 3 = 0.4666 ~= 0.5`  

(after rounding up, since we only have one digit after zero).  
Therefore, the grade for Objective 1 has become 0.5.
