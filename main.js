/* ------ DECLARATIONS ------ */

/**
 * Waits for a specific key (in this case 'Enter') to be pressed.
 * @param {key: number} key 
 */
function searchKeyPress(key) {
    // check if the code of the key pressed 
    // matches that of the 'Enter' key
    if (key.code === 'Enter') {
        // Execute the `addObjective` function
        addObjective();
        // return the boolean value `false` to the 
        // function that executed this one
        return false;
    }
    // return the boolean value `true` to the
    // function that executed this one
    return true;
}

/**
 * Adds an objective to the `objectives` list, calls the `updateObjectivesDiv`
 * function and resets the `objectivesInputField` to an empty String ("").
 */
function addObjective() {
    // get the input field with the id `objectiveInputId` and 
    // store it in the `objectiveInputField` variable
    var objectiveInputField = document.getElementById("objectiveInputId");
    // if the 'objectiveInputField value' is empty 
    // it returns to the calling function and no further actions will be performed
    if ( objectiveInputField.value === "") {
       return;
    }
    // add the value (the content) of the `objectiveInputField` to the
    // end of the global `objectives` list
    objectives.push(objectiveInputField.value);
    // execute the `updateObjectivesDiv` function
    updateObjectivesDiv();
    // set the value (content) of the `objectiveInputField`
    // to an empty String ("") so it no longer displays the newly
    // added objective and is ready for new enties
    objectiveInputField.value = "";
}

/**
 * Updates the div element with the id `objectivesDivId` by doing the following:
 * - Set the content (innerHTML) of the `div` with the id `objectivesDivId` to an empty String ("")
 * - Loops through the `objectives` list and for each element
 *  - Creates a new `div` element
 *  - Sets the styling of the new `div` element
 *  - Sets the content (innerHTML) of the `div` to that of the current objective
 *  - Adds the new `div` element with the set objective to the parent `div` with the id `objectivesDivId`
 */
function updateObjectivesDiv() {
    // gets the `div` element with the id `objectivesDivId` and
    // stores it in the `objectivesDiv` variable
    var objectivesDiv = document.getElementById("objectivesDivId")
    // sets the `innerHTML` value (content) of the `objectivesDiv`
    // element to an empty String ("") (it must be reset before adding new
    // elements, otherwise the same elements are added again every time)
    objectivesDiv.innerHTML = ""
    // iterates through all the elements in the global `objectives` list
    // passing in the different objectives in as the `entry` variable
    objectives.forEach(function(entry) {
        // creates a new `div` element and stores it in the `innerDiv` variable
        var innerDiv = document.createElement('div')
        // sets the style of the new `div` element (which is stored in the `innerDiv` variable)
        // according to what is desired
        innerDiv.style = "\
            background: #DF013A; \
            border: 1px; \
            border-style: solid; \
            border-color: gray; \
            width: 100px; \
            text-align: center; \
            padding: 3px; \
            border-radius: 7px; \
            display: inline-block; \
            margin: 2px; \
        "
        // saves the new objective String passed in as the `entry` variable
        // inside the innerHTML (content) of our newly created `div`, which we 
        // created and stored in the `innerDiv` variable earlier
        innerDiv.innerHTML = entry
        // the method `addEventListener` is called on the `innerDiv` element.
        // This method then adds an event listener to the element which will "listen"
        // for any 'click' performed on the `innerDiv`, which basically is the new objective (the red button)
        // and will then call the method `deleteObjective`.
        innerDiv.addEventListener('click', deleteObjective);
        // adds the newly created `div` element which we stored in the `innerDiv` variable
        // to the parent `div`, which we saved in the `objectivesDiv` variable
        objectivesDiv.appendChild(innerDiv)
    });
}

/**
 * Adds an event listener to the input field with the id `objectiveInputId`, which then
 * executes the `searchKeyPress` function for every `keyup` event.
 */
function main() {
    // gets the input field with the id `objectiveInputId` and stores it
    // in the variable with the name `objectiveInputField`
    var objectiveInputField = document.getElementById("objectiveInputId");
    // adds an event listener (listens for events that happen)
    // to the just retrieved `objectiveInputField` which !listens! for any 
    // `keyup` event (when a keyboard key is released) and when that happens, 
    // executes the previously defined `searchKeyPress` function
    objectiveInputField.addEventListener('keyup', searchKeyPress);
}

/**
 * Tells the `target` (objective div in our case) to delete/remove itself (the element that was clicked)
 * from its parent (the `objectivesDiv`). The event (a 'click' event in this case) knows what was clicked and has it stored
 * under `target`, which can be accessed by getting the property with `event.target`. The method `remove()` is available on the 
 * `target` element and can be called with `target.remove()`, telling the element the event was performed on (click on the objective)
 * to remove itself from the `objectivesDiv` (its parent).
 * @param {event: event} event 
 */
function deleteObjective(event) {
    var objective = event.innerHTML;
    var objectiveIndex = objectives.indexOf(objective);
    objectives.splice(objectiveIndex, 1);
    event.target.remove();
}
/* ----- DECLARATIONS END ----- */
/* ----- EXECUTION ON LOAD OF SCRIPT ----- */

// Initialize the globally available `objectives` list
// by setting it to an empty list
var objectives = []

// Call the main function when this (main.js) 
// script file is loaded
main()
